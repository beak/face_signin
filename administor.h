﻿#ifndef ADMINISTOR_H
#define ADMINISTOR_H

#include <QWidget>
#include "face_model.h"
#include <QCloseEvent>
namespace Ui {
class administor;
}

class administor : public QWidget
{
    Q_OBJECT

public:
    explicit administor(QWidget *parent = nullptr);
    ~administor();
    Person *add_person;
    QString end_time;

private slots:
    void on_admin_login_clicked();
    void on_add_stu_clicked();
    void on_btn_choice_clicked();
signals:
    void send_data(Person person);   //用来传递数据的信号
    void send_time_start(int flag);
protected:
     void closeEvent(QCloseEvent *event);
private:
    Ui::administor *ui;
};

#endif // ADMINISTOR_H
