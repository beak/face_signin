﻿#include "administor.h"
#include "ui_administor.h"
#include <QMessageBox>
#include <QFileDialog>

administor::administor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::administor)
{
    ui->setupUi(this);

    QTime time(7,50);
    ui->timeEdit_start->setTime(time);
    QTime time1(8,10);
    ui->timeEdit_end->setTime(time1);
    end_time = ui->timeEdit_end->text();

    ui->timeEdit_start->setEnabled(false);
    ui->timeEdit_end->setEnabled(false);
    ui->add_stu->setEnabled(false);
    ui->time_ok->setEnabled(false);
    //    add_person = new Person();
}

administor::~administor()
{
    delete ui;
}

void administor::on_admin_login_clicked()
{
    QString user_id =  this->ui->admin_id->text();
    QString user_passwd = this->ui->admin_passwd->text();
    if("1" == user_id && "1" == user_passwd)
    {
        ui->timeEdit_start->setEnabled(true);
        ui->timeEdit_end->setEnabled(true);
        ui->add_stu->setEnabled(true);
        ui->time_ok->setEnabled(true);
    }
    else
    {
        QMessageBox::information(NULL, "提示", "账号和密码都是1！");
    }
}

void administor::on_add_stu_clicked()
{
    //在信息有效且不为空的情况下发送添加的学生数据
    Person person;
    if(ui->name_cn_edit->text() !=""
       && ui->name_eng_edit->text() != ""
       && ui->id_edit->text() !=""
       && ui->class_edit_2->text() != "")
    {
        person.set_cn_name(ui->name_cn_edit->text());
        person.set_eng_name(ui->name_eng_edit->text());
        person.id_num = ui->id_edit->text();
        person.class_num = ui->class_edit_2->text();
        person.pic_path = ui->picpath_edit->text();
        person.sign_in = false;
        emit send_data(person);

        ui->name_cn_edit->setText("");
        ui->name_eng_edit->setText("");
        ui->id_edit->setText("");
        ui->class_edit_2->setText("");
        ui->picpath_edit->setText("");
        ui->admin_id->setText("");
        ui->admin_passwd->setText("");

        ui->timeEdit_start->setEnabled(false);
        ui->timeEdit_end->setEnabled(false);
        ui->add_stu->setEnabled(false);
        ui->time_ok->setEnabled(false);
        this->close();
    }
    else
    {
        QMessageBox::information(NULL, "提示", "请完善学生信息！");
    }
}

void administor::on_btn_choice_clicked()
{
    QString exe_path =  QCoreApplication::applicationDirPath();
    QString s = QFileDialog::getOpenFileName(
                    this, "选择人脸图片",
                    exe_path + "/data/face_lib",
                    "图片文件 (*.jpg);; 所有文件 (*.*);; ");
        //qDebug() << "path=" << s;
        if (!s.isEmpty())
        {
            ui->picpath_edit->setText(s);
        }
}
void administor::closeEvent(QCloseEvent *event)
{
    int flag = 1;
    emit send_time_start(flag);
}
