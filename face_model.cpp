﻿#include "face_model.h"
#include<QDebug>
#include<QString>
#include<QFile>
#include<QMessageBox>
#include<QCoreApplication>



void Person::set_cn_name(QString name)
{
    name_cn = name;
}
void Person::set_eng_name(QString name)
{
    name_eng = name;
}
void Person::set_idnum(QString name)
{
    id_num = name;
}
void Person::set_face_feature(dlib::matrix<float,0,1> features)
{
    face_feature = features;
}


FaceDetector::FaceDetector()
{
    QString exe_path =  QCoreApplication::applicationDirPath();
    xml_path = exe_path +"/data/haarcascade_frontalface_alt2.xml";
    face_landmarks_path = exe_path + "/data/shape_predictor_68_face_landmarks.dat";
    resnet_model_path = exe_path + "/data/dlib_face_recognition_resnet_model_v1.dat";
    pic_dir_path = exe_path + "/data/face_info.txt";
    dir_path = exe_path + "/data/face_lib";
    add_face_batch(pic_dir_path);
}
bool FaceDetector::add_face(Person person)
{
      qDebug()<<person.name_cn;
      qDebug()<<person.name_eng;
      qDebug()<<person.pic_path;

//        qDebug()<< strList[0]<<strList[1]<<strList[2]<<strList[3];
     QString write_txt = person.name_cn + " " +
             person.name_eng + " " +
             person.id_num + " " +              //person.class_num + " " +
             person.pic_path.split("/")[person.pic_path.split("/").size()-1] + " ";
     QFile file(pic_dir_path);

     if (file.open(QIODevice::ReadWrite | QIODevice::Text))
     {
         QTextStream stream(&file);
         stream.seek(file.size());

         stream <<"\n"<<write_txt;
         file.close();
     }
      // 构建人脸库信息
        Person *person_temp = new Person;
        person_temp->set_cn_name(person.name_cn);
        person_temp->set_eng_name(person.name_eng);
        person_temp->set_idnum(person.id_num);
        person_temp->class_num = person.class_num;

        cv::Mat src;
        cv::Mat frame = cv::imread(person.pic_path.toStdString());
        cv::cvtColor(frame, src, cv::COLOR_BGR2GRAY);

        dlib::array2d<dlib::bgr_pixel> dimg;
        dlib::assign_image(dimg, dlib::cv_image<uchar>(src));

        //haar级联分类器探测人脸区域，获取一系列人脸所在区域
        std::vector<cv::Rect> objects;
        std::vector<dlib::rectangle> dets;
        face_detector.detectMultiScale(src, objects);
        for (int i = 0; i < objects.size(); i++)
        {
           // cv::rectangle(frame, objects[i], CV_RGB(200,0,0));
            dlib::rectangle r(objects[i].x, objects[i].y, objects[i].x + objects[i].width, objects[i].y + objects[i].height);
            if(r.area()>10000)
                dets.push_back(r);  //正常情况下应该只检测到一副面容
        }
//        if (dets.size() == 0)
//            continue;

        std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
        std::vector<dlib::full_object_detection> shapes;

        for(int i = 0; i < dets.size(); i++)
        {
            dlib::full_object_detection shape = sp(dimg, dets[i]); //获取指定一个区域的人脸形状
            shapes.push_back(shape);

            dlib::matrix<dlib::rgb_pixel> face_chip;
            dlib::extract_image_chip(dimg, dlib::get_face_chip_details(shape,150,0.25), face_chip);

            faces.push_back(std::move(face_chip));
        }
        if (faces.size() == 0)
        {
           // qDebug()<< "No faces found in " <<person_temp->name_eng<<endl;
           ;// continue;
        }
        std::vector<dlib::matrix<float,0,1>> face_descriptors = net(faces);
        //qDebug()<<face_descriptors.size();
        person_temp->set_face_feature(face_descriptors[0]);
        fdlib.push_back(*person_temp);
}

bool FaceDetector::add_face_batch(QString pic_dir_path)
{
    face_detector.load(xml_path.toStdString());
    dlib::deserialize(face_landmarks_path.toStdString()) >> sp;
    dlib::deserialize(resnet_model_path.toStdString()) >> net;

    QFile file(pic_dir_path);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        //qDebug()<<"Can't open the file!"<<endl;
    }
    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        QString str(line);
        QStringList strList = str.split(" ");
        qDebug()<< strList[0]<<strList[1]<<strList[2]<<strList[3];
        // 构建人脸库信息
        Person *person_temp = new Person;

        person_temp->set_cn_name(strList[0]);
        person_temp->set_eng_name(strList[1]);
        person_temp->set_idnum(strList[2]);
        //qDebug()<<person_temp->name_cn<<person_temp->name_eng<<person_temp->id_num;

        cv::Mat src;
        std::string path= strList[3].toStdString();
        //qDebug()<<"PATH:"<<strList[3];
        QString qstr2 = QString::fromStdString(path);

        //qDebug()<<dir_path + qstr2;

        cv::Mat frame = cv::imread(dir_path.toStdString()+"/"+path);
        cv::cvtColor(frame, src, cv::COLOR_BGR2GRAY);

//        cv::imshow("1",src);
//        cv::waitKey(2000);

        dlib::array2d<dlib::bgr_pixel> dimg;
        dlib::assign_image(dimg, dlib::cv_image<uchar>(src));

        //haar级联分类器探测人脸区域，获取一系列人脸所在区域
        std::vector<cv::Rect> objects;
        std::vector<dlib::rectangle> dets;
        face_detector.detectMultiScale(src, objects);
        for (int i = 0; i < objects.size(); i++)
        {
           // cv::rectangle(frame, objects[i], CV_RGB(200,0,0));
            dlib::rectangle r(objects[i].x, objects[i].y, objects[i].x + objects[i].width, objects[i].y + objects[i].height);
            if(r.area()>10000)
                dets.push_back(r);  //正常情况下应该只检测到一副面容
        }
        if (dets.size() == 0)
            continue;

        std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
        std::vector<dlib::full_object_detection> shapes;

        for(int i = 0; i < dets.size(); i++)
        {
            dlib::full_object_detection shape = sp(dimg, dets[i]); //获取指定一个区域的人脸形状
            shapes.push_back(shape);

            dlib::matrix<dlib::rgb_pixel> face_chip;
            dlib::extract_image_chip(dimg, dlib::get_face_chip_details(shape,150,0.25), face_chip);

            faces.push_back(std::move(face_chip));
        }
        if (faces.size() == 0)
        {
           // qDebug()<< "No faces found in " <<person_temp->name_eng<<endl;
            continue;
        }
        std::vector<dlib::matrix<float,0,1>> face_descriptors = net(faces);
        //qDebug()<<face_descriptors.size();
        person_temp->set_face_feature(face_descriptors[0]);
        fdlib.push_back(*person_temp);
    }
}

bool FaceDetector::recog_face(cv::Mat &img,Person &person)
{
    cv::Mat src_test;

    cv::cvtColor(img, src_test, cv::COLOR_BGR2GRAY);
    dlib::array2d<dlib::bgr_pixel> dimg;
    dlib::assign_image(dimg, dlib::cv_image<uchar>(src_test));

    //haar级联分类器探测人脸区域，获取一系列人脸所在区域
    std::vector<cv::Rect> objects;
    std::vector<cv::Rect> objects_temp;
    std::vector<dlib::rectangle> dets;
    face_detector.detectMultiScale(src_test, objects_temp);

    for(int i = 0;i < objects_temp.size();i++)
    {
        if(objects_temp[i].area()<10000)
        {
            continue;
        }
        objects.push_back(objects_temp[i]);
    }
    for (int i = 0; i < objects.size(); i++)
    {
        dlib::rectangle r(objects[i].x, objects[i].y, objects[i].x + objects[i].width, objects[i].y + objects[i].height);
        if(r.area()<10000)
        {
            continue;
        }

        cv::rectangle(img, objects[i], CV_RGB(200,0,0));
        dets.push_back(r);  //正常情况下应该只检测到一副面容
    }
    if (dets.size() == 0)
    {
        //qDebug()<< "there is no faces found in "<<endl;
        return false;
    }
    std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
    std::vector<dlib::full_object_detection> shapes;

    for(int i = 0; i < dets.size(); i++)
    {
        dlib::full_object_detection shape = sp(dimg, dets[i]); //获取指定一个区域的人脸形状
        shapes.push_back(shape);

        dlib::matrix<dlib::rgb_pixel> face_chip;
        dlib::extract_image_chip(dimg, dlib::get_face_chip_details(shape,150,0.25), face_chip);

        faces.push_back(std::move(face_chip));
    }
    if (faces.size() == 0)
    {
       //qDebug()<< "No faces found in " <<endl;
       return false;
    }
    line_one_face_detections(img, shapes);
    std::vector<dlib::matrix<float,0,1>> face_descriptors = net(faces);

    float min_dis = 1.0;
    std::string temp_name = "unknow";
    int person_num = -1;
//    for(std::vector<Person>::iterator it = fdlib.begin();it != fdlib.end();it++)
    for(int i = 0;i < fdlib.size(); i++)
    {
        float distance = length(fdlib[i].face_feature - face_descriptors[0]);
//        qDebug()<< "the pic is " << it->name_cn << "!, distance:" << distance << endl;
        if( distance < 0.35 )
        {
            if(distance <= min_dis)
            {
                min_dis = distance;
                temp_name = fdlib[i].name_eng.toStdString();
                person_num = i;
            }
         }
     }
    cv::Point org(objects[0].x, objects[0].y);
    //判断下是否已经签到
    if(person_num != -1)
    {
        //
        if(fdlib[person_num].sign_in != true)
        {
            //向签到表写入数据
            fdlib[person_num].sign_in = true;
            cv::putText(img, temp_name, org, cv::FONT_HERSHEY_SIMPLEX, 2.0, CV_RGB(0, 200,0 ),2);
//            QMessageBox::information(NULL, "Title", fdlib[person_num].name_cn,
//                                     QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
           // qDebug()<<fdlib[person_num].name_cn<<"签到";
            person = fdlib[person_num];
            return true;
        }
        else
        {
            cv::putText(img, temp_name+"signed in", org, cv::FONT_HERSHEY_SIMPLEX, 2.0, CV_RGB(0, 200, 200),2);
           // qDebug()<<fdlib[person_num].name_cn<<"已经签到";
            return false;
        }
    }
    else
    {
        cv::putText(img, temp_name, org, cv::FONT_HERSHEY_SIMPLEX, 2.0, CV_RGB(0, 0, 200),2);
        return false;
    }
    return false;
}
void FaceDetector::GetFiles(std::string path, std::map<std::string, std::string> &files)
{
    ;
}
void  FaceDetector::line_one_face_detections(cv::Mat img, std::vector<dlib::full_object_detection> fs)
{
    int i, j;

    for(j=0; j<fs.size(); j++)
    {
        cv::Point p1, p2;

        for(i = 0; i<67; i++)
        {
            // 下巴到脸颊 0 ~ 16
            //左边眉毛 17 ~ 21
            //右边眉毛 21 ~ 26
            //鼻梁     27 ~ 30
            //鼻孔        31 ~ 35
            //左眼        36 ~ 41
            //右眼        42 ~ 47
            //嘴唇外圈  48 ~ 59
            //嘴唇内圈  59 ~ 67
            switch(i)
            {
                case 16:
                case 21:
                case 26:
                case 30:
                case 35:
                case 41:
                case 47:
                case 59:
                    i++;
                    break;
                default:
                    break;
            }
            p1.x = fs[j].part(i).x();
            p1.y = fs[j].part(i).y();
            p2.x = fs[j].part(i+1).x();
            p2.y = fs[j].part(i+1).y();
            cv::line(img, p1, p2, cv::Scalar(0,255,255), 2);
        }

    }
}
