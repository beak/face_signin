﻿#ifndef ADMIN_H
#define ADMIN_H

#include <QWidget>
#include "dialog.h"
namespace Ui {
class admin;
}

class admin : public QWidget
{
    Q_OBJECT

public:
    explicit admin(QWidget *parent = nullptr);
    ~admin();
    QString start_time;
    QString end_time;
private slots:
    void on_admin_login_clicked();
    void on_time_ok_clicked();
    void on_add_member_clicked();

private:
    Ui::admin *ui;
    Dialog *add_person_info;
};

#endif // ADMIN_H
