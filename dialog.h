﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();
    QString name_cn;
    QString name_eng;
    QString id_num;
    QString class_name;
    QString pic_path;
private slots:
    void on_btn_choice_clicked();

    void on_btn_ok_clicked();

    void on_btn_cancel_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
