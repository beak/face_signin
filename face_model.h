﻿#ifndef FACE_MODEL_H
#define FACE_MODEL_H
#include <iostream>
#include <vector>
#include <ctime>
#include <string>
#include <map>
#include <io.h>

#include <opencv2/opencv.hpp>
#include <QString>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/opencv.h>
#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <dlib/clustering.h>
#include <dlib/string.h>




template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual = dlib::add_prev1<block<N,BN,1,dlib::tag1<SUBNET>>>;

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual_down = dlib::add_prev2<dlib::avg_pool<2,2,2,2,dlib::skip1<dlib::tag2<block<N,BN,2,dlib::tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block  = BN<dlib::con<N,3,3,1,1,dlib::relu<BN<dlib::con<N,3,3,stride,stride,SUBNET>>>>>;

template <int N, typename SUBNET> using ares      = dlib::relu<residual<block,N,dlib::affine,SUBNET>>;
template <int N, typename SUBNET> using ares_down = dlib::relu<residual_down<block,N,dlib::affine,SUBNET>>;

template <typename SUBNET> using alevel0 = ares_down<256,SUBNET>;
template <typename SUBNET> using alevel1 = ares<256,ares<256,ares_down<256,SUBNET>>>;
template <typename SUBNET> using alevel2 = ares<128,ares<128,ares_down<128,SUBNET>>>;
template <typename SUBNET> using alevel3 = ares<64,ares<64,ares<64,ares_down<64,SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<32,ares<32,ares<32,SUBNET>>>;

using anet_type = dlib::loss_metric<dlib::fc_no_bias<128,dlib::avg_pool_everything<
                            alevel0<
                            alevel1<
                            alevel2<
                            alevel3<
                            alevel4<
                            dlib::max_pool<3,3,2,2,dlib::relu<dlib::affine<dlib::con<32,7,7,2,2,
                            dlib::input_rgb_image_sized<150>
                            >>>>>>>>>>>>;


class Person
{
public:
    Person()
    {
        sign_in = false;
    }
    void set_cn_name(QString name);
    void set_eng_name(QString name);
    void set_idnum(QString name);
    void set_face_feature(dlib::matrix<float,0,1> features);

    //QString
    QString name_eng;
    QString name_cn;
    QString id_num;
    QString class_num;
    QString pic_path;
    dlib::matrix<float,0,1> face_feature;
    bool sign_in;
private:
//    std::string name_eng;
//    std::string name_cn;
//    std::string id_num;
//    dlib::matrix<float,0,1> face_feature;

};


class FaceDetector
{
public:
    FaceDetector();
    bool recog_face(cv::Mat &img,Person &person);


public:
    cv::CascadeClassifier face_detector;
    dlib::shape_predictor sp;
    anet_type net;

    std::vector<Person> fdlib;
    QString xml_path;
    QString face_landmarks_path;
    QString resnet_model_path;
    QString pic_dir_path;
    QString dir_path;
public:
    bool add_face(Person person);
    bool add_face_batch(QString pic_dir_path);
    void GetFiles(std::string path, std::map<std::string, std::string> &files);
    void line_one_face_detections(cv::Mat img, std::vector<dlib::full_object_detection> fs);

private:

//    std::string xml_path = "E:\\project\\earn_my_crust\\face_recognition\\data\\model\\haarcascade_frontalface_alt2.xml";
//    std::string face_landmarks_path = "E:\\project\\earn_my_crust\\face_recognition\\data\\model\\shape_predictor_68_face_landmarks.dat";
//    std::string resnet_model_path = "E:\\project\\earn_my_crust\\face_recognition\\data\\model\\dlib_face_recognition_resnet_model_v1.dat";

};

#endif // FACE_MODEL_H
