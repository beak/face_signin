QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS_RELEASE += -O3       # Release -O3
# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    administor.cpp \
    dialog.cpp \
    face_model.cpp \
    form.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    administor.h \
    dialog.h \
    face_model.h \
    form.h \
    widget.h

FORMS += \
    administor.ui \
    dialog.ui \
    form.ui \
    widget.ui




LIBS+=F:/software/IDE/dlib/build/lib/libdlib.a

INCLUDEPATH+=F:/software/IDE/dlib/build/include

INCLUDEPATH += F:\software\IDE\opencv\opencv-4.4.0\op_build\install\include
LIBS += F:\software\IDE\opencv\opencv-4.4.0\op_build\install\x64\mingw\bin\libopencv_*.dll
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES +=

DISTFILES +=
